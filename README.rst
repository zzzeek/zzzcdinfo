===========================
Get Tracks from Musicbrainz
===========================

So far, this is doing everything it needs to be dropped
in with abcde as the contents of abcde-musicbrainz-tool.

However, abcde itself is so crufty that it's likely I'll
just make this thing rip the .wav / .mp3 file and tag them directly
in any case.