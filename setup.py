from setuptools import setup, find_packages

requires = [
    'musicbrainzngs',
    'discid'
]

setup(name='zzzcdinfo',
      version='0.1',
      description="cd information extractor",
      long_description="mike's cd information extractor",
      classifiers=[
          'Environment :: Console',
          'Programming Language :: Python',
      ],
      author='Mike Bayer',
      author_email='mike@zzzcomputing.com',
      url='http://bitbucket.org/zzzeek/zzzcdinfo',
      license='MIT',
      packages=find_packages('.', exclude=['examples*', 'test*']),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      entry_points={
          'console_scripts': ['musicbrainz-get-tracks = zzzcdinfo.musicbrainz:main'],
      }
      )
